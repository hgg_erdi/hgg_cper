import numpy as np
import copy
from envs.utils import quaternion_to_euler_angle

def goal_concat(obs, goal):
	return np.concatenate([obs, goal], axis=0)

def goal_based_process(obs):
	return goal_concat(obs['observation'], obs['desired_goal'])

class Trajectory:
	def __init__(self, init_obs):
		self.ep = {
			'obs': [copy.deepcopy(init_obs)],
			'rews': [],
			'acts': [],
			'done': [],
			'info': []
		}
		self.length = 0

	def store_step(self, action, obs, reward, done, info):
		self.ep['acts'].append(copy.deepcopy(action))
		self.ep['obs'].append(copy.deepcopy(obs))
		self.ep['rews'].append(copy.deepcopy([reward]))
		self.ep['done'].append(copy.deepcopy([np.float32(done)]))
		self.ep['info'].append(copy.deepcopy([info]))
		self.length += 1

	def energy(self, env_id, w_potential=1.0, w_linear=1.0, w_rotational=1.0):
		# from "Energy-Based Hindsight Experience Prioritization"
		if env_id[:5]=='Fetch':
			obj = []
			for i in range(len(self.ep['obs'])):
				obj.append(self.ep['obs'][i]['achieved_goal'])
			obj = np.array([obj])

			clip_energy = 0.5
			height = obj[:, :, 2]
			height_0 = np.repeat(height[:,0].reshape(-1,1), height[:,1::].shape[1], axis=1)
			height = height[:,1::] - height_0
			g, m, delta_t = 9.81, 1, 0.04
			potential_energy = g*m*height
			diff = np.diff(obj, axis=1)
			velocity = diff / delta_t
			kinetic_energy = 0.5 * m * np.power(velocity, 2)
			kinetic_energy = np.sum(kinetic_energy, axis=2)
			energy_totoal = w_potential*potential_energy + w_linear*kinetic_energy
			energy_diff = np.diff(energy_totoal, axis=1)
			energy_transition = energy_totoal.copy()
			energy_transition[:,1::] = energy_diff.copy()
			energy_transition = np.clip(energy_transition, 0, clip_energy)
			energy_transition_total = np.sum(energy_transition, axis=1)
			energy_final = energy_transition_total.reshape(-1,1)
			return np.sum(energy_final)
		else:
			assert env_id[:4]=='Hand'
			obj = []
			for i in range(len(self.ep['obs'])):
				obj.append(self.ep['obs'][i]['observation'][-7:])
			obj = np.array([obj])

			clip_energy = 2.5
			g, m, delta_t, inertia  = 9.81, 1, 0.04, 1
			quaternion = obj[:,:,3:].copy()
			angle = np.apply_along_axis(quaternion_to_euler_angle, 2, quaternion)
			diff_angle = np.diff(angle, axis=1)
			angular_velocity = diff_angle / delta_t
			rotational_energy = 0.5 * inertia * np.power(angular_velocity, 2)
			rotational_energy = np.sum(rotational_energy, axis=2)
			obj = obj[:,:,:3]
			height = obj[:, :, 2]
			height_0 = np.repeat(height[:,0].reshape(-1,1), height[:,1::].shape[1], axis=1)
			height = height[:,1::] - height_0
			potential_energy = g*m*height
			diff = np.diff(obj, axis=1)
			velocity = diff / delta_t
			kinetic_energy = 0.5 * m * np.power(velocity, 2)
			kinetic_energy = np.sum(kinetic_energy, axis=2)
			energy_totoal = w_potential*potential_energy + w_linear*kinetic_energy + w_rotational*rotational_energy
			energy_diff = np.diff(energy_totoal, axis=1)
			energy_transition = energy_totoal.copy()
			energy_transition[:,1::] = energy_diff.copy()
			energy_transition = np.clip(energy_transition, 0, clip_energy)
			energy_transition_total = np.sum(energy_transition, axis=1)
			energy_final = energy_transition_total.reshape(-1,1)
			return np.sum(energy_final)

class ReplayBuffer_Episodic:
	def __init__(self, args):
		self.args = args
		if args.buffer_type=='energy':
			self.energy = True
			self.energy_sum = 0.0
			self.energy_offset = 0.0
			self.energy_max = 1.0
		else:
			self.energy = False
		self.buffer = {}
		self.steps = []
		self.length = 0
		self.counter = 0
		self.steps_counter = 0
		self.sample_methods = {
			'ddpg': self.sample_batch_ddpg
		}
		self.sample_batch = self.sample_methods[args.alg]
		self.force_obs = np.array([])

	def store_trajectory(self, trajectory):
		episode = trajectory.ep
		if self.energy:
			energy = trajectory.energy(self.args.env)
			self.energy_sum += energy
		if self.counter==0:
			for key in episode.keys():
				self.buffer[key] = []
			if self.energy:
				self.buffer_energy = []
				self.buffer_energy_sum = []
		if self.counter<self.args.buffer_size:
			for key in self.buffer.keys():
				self.buffer[key].append(episode[key])
			if self.energy:
				self.buffer_energy.append(copy.deepcopy(energy))
				self.buffer_energy_sum.append(copy.deepcopy(self.energy_sum))
			self.length += 1
			self.steps.append(trajectory.length)
		else:
			idx = self.counter%self.args.buffer_size
			for key in self.buffer.keys():
				self.buffer[key][idx] = episode[key]
			if self.energy:
				self.energy_offset = copy.deepcopy(self.buffer_energy_sum[idx])
				self.buffer_energy[idx] = copy.deepcopy(energy)
				self.buffer_energy_sum[idx] = copy.deepcopy(self.energy_sum)
			self.steps[idx] = trajectory.length
		self.counter += 1
		self.steps_counter += trajectory.length

	def energy_sample(self):
		t = self.energy_offset + np.random.uniform(0,1)*(self.energy_sum-self.energy_offset)
		if self.counter>self.args.buffer_size:
			if self.buffer_energy_sum[-1]>=t:
				return self.energy_search(t, self.counter%self.length, self.length-1)
			else:
				return self.energy_search(t, 0, self.counter%self.length-1)
		else:
			return self.energy_search(t, 0, self.length-1)

	def energy_search(self, t, l, r):
		if l==r: return l
		mid = (l+r)//2
		if self.buffer_energy_sum[mid]>=t:
			return self.energy_search(t, l, mid)
		else:
			return self.energy_search(t, mid+1, r)

	def goal_distance(self, goal_a, goal_b):
		assert goal_a.shape == goal_b.shape
		try:
			return np.linalg.norm(goal_a - goal_b, axis=-1)
		except(IndexError, ValueError):
			return np.linalg.norm(goal_a - goal_b)


	def compute_reward(self, achieved_goal, goal, info):
		# weights
		force_weight = 0.25
		position_weight = 1 - force_weight

		# Compute rewards
		intrinsic = info['intrinsic_sum_force']

		force_reward = -(np.squeeze(intrinsic) < 10).astype(np.float32)
		d_pos = self.goal_distance(achieved_goal, goal)
		position_reward = -(d_pos > 0.05).astype(np.float32)

		return force_weight * force_reward + position_weight * position_reward

	def sample_batch_ddpg(self, batch_size=-1, normalizer=False, plain=False, train_state=False):
		assert int(normalizer) + int(plain) <= 1
		if batch_size == -1: batch_size = self.args.batch_size
		batch = dict(obs=[], obs_next=[], acts=[], rews=[], done=[])

		rollout_batch_size = len(self.buffer['acts'])
		T = len(self.buffer['acts'][0])

		if not train_state:
			for timestep_number in range(len(self.buffer['obs'][rollout_batch_size - 1 ])):
				self.force_obs = np.append(self.force_obs, self.buffer['obs'][rollout_batch_size - 1 ][timestep_number]['observation'][-1])

		try:
			self.force_obs = self.force_obs.reshape(len(self.buffer['obs']), -1)
		except:
			print('error')
		force_obs_probability = 9 * (self.force_obs > 1).astype(np.float32) + 1


		force_obs_probability_episode = np.sum(force_obs_probability, axis=1)
		force_obs_probability_episode = force_obs_probability_episode / np.sum(force_obs_probability_episode)
		episode_idxs = np.random.choice(rollout_batch_size, size=batch_size, p=force_obs_probability_episode)

		force_obs_probability_step = force_obs_probability[episode_idxs, :]
		force_obs_probability_step = np.delete(force_obs_probability_step, -1, axis=1)
		row_sums = np.sum(force_obs_probability_step, axis=1)
		force_obs_probability_step = force_obs_probability_step / row_sums[:, np.newaxis]

		t_samples = np.array(
			[np.random.choice(T, size=1, p=force_obs_probability_step_row) for
			 force_obs_probability_step_row
			 in list(force_obs_probability_step)])
		t_samples = np.array(t_samples).squeeze()

		for i in range(batch_size):
			if self.energy:
				#idx = self.energy_sample()
				idx = episode_idxs[i]
			else:
				idx = np.random.randint(self.length)
			step = np.random.randint(self.steps[idx])

			if self.args.goal_based:
				if plain:
					# no additional tricks
					goal = self.buffer['obs'][idx][step]['desired_goal']
				elif normalizer:
					# uniform sampling for normalizer update
					goal = self.buffer['obs'][idx][step]['achieved_goal']
				else:
					# upsampling by HER trick
					if (self.args.her != 'none') and (np.random.uniform() <= self.args.her_ratio):
						if self.args.her == 'match':
							goal = self.args.goal_sampler.sample()
							goal_pool = np.array([obs['achieved_goal'] for obs in self.buffer['obs'][idx][step + 1:]])
							step_her = (step + 1) + np.argmin(np.sum(np.square(goal_pool - goal), axis=1))
							goal = self.buffer['obs'][idx][step_her]['achieved_goal']
						else:
							step_her = {
								'final': self.steps[idx],
								'future': np.random.randint(step + 1, self.steps[idx] + 1)
							}[self.args.her]
							goal = self.buffer['obs'][idx][step_her]['achieved_goal']
					else:
						goal = self.buffer['obs'][idx][step]['desired_goal']

				achieved = self.buffer['obs'][idx][step + 1]['achieved_goal']
				achieved_old = self.buffer['obs'][idx][step]['achieved_goal']
				obs = goal_concat(self.buffer['obs'][idx][step]['observation'], goal)
				obs_next = goal_concat(self.buffer['obs'][idx][step + 1]['observation'], goal)
				act = self.buffer['acts'][idx][step]
				info = self.buffer['info'][idx][step]
				rew = self.args.compute_reward((achieved, achieved_old), goal, info)
				done = self.buffer['done'][idx][step]

				batch['obs'].append(copy.deepcopy(obs))
				batch['obs_next'].append(copy.deepcopy(obs_next))
				batch['acts'].append(copy.deepcopy(act))
				batch['rews'].append(copy.deepcopy([rew]))
				batch['done'].append(copy.deepcopy(done))
			else:
				for key in ['obs', 'acts', 'rews', 'done']:
					if key == 'obs':
						batch['obs'].append(copy.deepcopy(self.buffer[key][idx][step]))
						batch['obs_next'].append(copy.deepcopy(self.buffer[key][idx][step + 1]))
					else:
						batch[key].append(copy.deepcopy(self.buffer[key][idx][step]))

		return batch

		#
		# if batch_size == -1: batch_size = self.args.batch_size
		# replay_k = 4
		# future_p = 1 - (1. / (1 + replay_k))
		#
		#
		# rollout_batch_size = len(self.buffer['acts'])
		# T = len(self.buffer['acts'][0])
		# batch_size = batch_size
		#
		# if rollout_batch_size>198:
		# 	print('stop')
		# if not train_state:
		# 	for timestep_number in range(len(self.buffer['obs'][rollout_batch_size - 1 ])):
		# 		self.force_obs = np.append(self.force_obs, self.buffer['obs'][rollout_batch_size - 1 ][timestep_number]['observation'][-1])
		#
		# try:
		# 	self.force_obs = self.force_obs.reshape(len(self.buffer['obs']), -1)
		# except:
		# 	print('error')
		# force_obs_probability = 9 * (self.force_obs > 1).astype(np.float32) + 1
		#
		#
		# force_obs_probability_episode = np.sum(force_obs_probability, axis=1)
		# force_obs_probability_episode = force_obs_probability_episode / np.sum(force_obs_probability_episode)
		# episode_idxs = np.random.choice(rollout_batch_size, size=batch_size, p=force_obs_probability_episode)
		#
		# force_obs_probability_step = force_obs_probability[episode_idxs, :]
		# force_obs_probability_step = np.delete(force_obs_probability_step, -1, axis=1)
		# row_sums = np.sum(force_obs_probability_step, axis=1)
		# force_obs_probability_step = force_obs_probability_step / row_sums[:, np.newaxis]
		#
		# t_samples = np.array(
		# 	[np.random.choice(T, size=1, p=force_obs_probability_step_row) for
		# 	 force_obs_probability_step_row
		# 	 in list(force_obs_probability_step)])
		# t_samples = np.array(t_samples).squeeze()
		#
		#
		# her_indexes = np.where(np.random.uniform(size=batch_size) < future_p)
		# past_offset = np.random.uniform(size=batch_size) * t_samples
		# past_offset = past_offset.astype(int)
		# future_t = (t_samples + 1)[her_indexes]
		#
		#
		# future_ag_tmp = np.array(self.buffer['obs'])[episode_idxs[her_indexes], future_t]
		#
		# future_ag = np.array([])
		#
		# for i in range(len(future_ag_tmp)):
		# 	future_ag = np.append(future_ag,future_ag_tmp[i]['achieved_goal'])
		#
		# future_ag = future_ag.reshape(-1, 3)
		#
		# transitions = {}
		# transitions['o'] = np.array([self.buffer['obs'][episode_idxs[i]][past_offset[i]]['observation']  for i in range(len(past_offset)) ])
		# transitions['o_next'] = np.array([self.buffer['obs'][episode_idxs[i]][past_offset[i]+1]['observation'] for i in range(len(past_offset))])
		# transitions['g'] = np.array([self.buffer['obs'][episode_idxs[i]][past_offset[i]]['desired_goal'] for i in range(len(past_offset))])
		# transitions['ag'] = np.array([self.buffer['obs'][episode_idxs[i]][past_offset[i]]['achieved_goal'] for i in range(len(past_offset))])
		# transitions['info'] = np.array([self.buffer['info'][episode_idxs[i]][past_offset[i]] for i in range(len(past_offset))])
		# transitions['done'] = np.array([self.buffer['done'][episode_idxs[i]][past_offset[i]] for i in range(len(past_offset))])
		# transitions['acts'] = np.array([self.buffer['acts'][episode_idxs[i]][past_offset[i]] for i in range(len(past_offset))])
		#
		# transitions['g'][her_indexes] = future_ag
		#
		# reward_params = {k: transitions[k] for k in ['ag', 'g']}
		# reward_params['info'] = {}
		# reward_params['info']['is_success'] = np.array([])
		# reward_params['info']['intrinsic_force'] = np.array([])
		# reward_params['info']['intrinsic_sum_force'] = np.array([])
		# reward_params['info']['intrinsic_sum_energy'] = np.array([])
		# for i in range(len(transitions['info'])):
		# 	reward_params['info']['is_success'] = np.append(reward_params['info']['is_success'],transitions['info'][i][0]['is_success'])
		# 	reward_params['info']['intrinsic_force'] = np.append(reward_params['info']['intrinsic_force'],transitions['info'][i][0]['intrinsic_force'])
		# 	reward_params['info']['intrinsic_sum_force'] = np.append(reward_params['info']['intrinsic_sum_force'],transitions['info'][i][0]['intrinsic_sum_force'])
		# 	reward_params['info']['intrinsic_sum_energy'] = np.append(reward_params['info']['intrinsic_sum_energy'],transitions['info'][i][0]['intrinsic_sum_energy'])
		#
		# reward_params['info']['intrinsic_sum_energy'] = reward_params['info']['intrinsic_sum_energy'].reshape(-1, 3)
		#
		# transitions['r'] = self.compute_reward(reward_params['ag'], reward_params['g'], reward_params['info'])
		#
		# batch = dict(obs=[], obs_next=[], acts=[], rews=[], done=[])
		# batch['obs'] = copy.deepcopy(np.concatenate((transitions['o'],transitions['g']),axis=1).tolist())
		# batch['obs_next'] = copy.deepcopy(np.concatenate((transitions['o_next'],transitions['g']),axis=1).tolist())
		# batch['acts'] = copy.deepcopy(transitions['acts'].tolist())
		# batch['rews'] = copy.deepcopy(transitions['r'].tolist())
		# batch['done'] = copy.deepcopy(transitions['done'].tolist())
		#
		# return batch
		# assert int(normalizer) + int(plain) <= 1
		# if batch_size==-1: batch_size = self.args.batch_size
		# batch = dict(obs=[], obs_next=[], acts=[], rews=[], done=[])
		#
		# for i in range(batch_size):
		# 	if self.energy:
		# 		idx = self.energy_sample()
		# 	else:
		# 		idx = np.random.randint(self.length)
		# 	step = np.random.randint(self.steps[idx])
		#
		# 	if self.args.goal_based:
		# 		if plain:
		# 			# no additional tricks
		# 			goal = self.buffer['obs'][idx][step]['desired_goal']
		# 		elif normalizer:
		# 			# uniform sampling for normalizer update
		# 			goal = self.buffer['obs'][idx][step]['achieved_goal']
		# 		else:
		# 			# upsampling by HER trick
		# 			if (self.args.her!='none') and (np.random.uniform()<=self.args.her_ratio):
		# 				if self.args.her=='match':
		# 					goal = self.args.goal_sampler.sample()
		# 					goal_pool = np.array([obs['achieved_goal'] for obs in self.buffer['obs'][idx][step+1:]])
		# 					step_her = (step+1) + np.argmin(np.sum(np.square(goal_pool-goal),axis=1))
		# 					goal = self.buffer['obs'][idx][step_her]['achieved_goal']
		# 				else:
		# 					step_her = {
		# 						'final': self.steps[idx],
		# 						'future': np.random.randint(step+1, self.steps[idx]+1)
		# 					}[self.args.her]
		# 					goal = self.buffer['obs'][idx][step_her]['achieved_goal']
		# 			else:
		# 				goal = self.buffer['obs'][idx][step]['desired_goal']
		#
		# 		achieved = self.buffer['obs'][idx][step+1]['achieved_goal']
		# 		achieved_old = self.buffer['obs'][idx][step]['achieved_goal']
		# 		obs = goal_concat(self.buffer['obs'][idx][step]['observation'], goal)
		# 		obs_next = goal_concat(self.buffer['obs'][idx][step+1]['observation'], goal)
		# 		act = self.buffer['acts'][idx][step]
		# 		rew = self.args.compute_reward((achieved, achieved_old), goal)
		# 		done = self.buffer['done'][idx][step]
		#
		#
		#
		# 		# self.buffer['obs'][episode_number][timestep_number]['observation'].shape
		# 		# force_obs = self.buffer['obs'][0][:]['observation'][-1]
		# 		# force_obs_probability = 9 * (force_obs > 1).astype(np.float32) + 1
		# 		#
		# 		# # Compute probabilities for episode ...
		# 		# force_obs_probability_episode = np.sum(force_obs_probability, axis=1)
		# 		# force_obs_probability_episode = force_obs_probability_episode / np.sum(force_obs_probability_episode)
		# 		force_obs = np.array([])
		# 		for episode_number in range(len(self.buffer['obs'])):
		# 			for timestep_number in range(len(self.buffer['obs'][episode_number])):
		# 				force_obs = np.append(force_obs,self.buffer['obs'][episode_number][timestep_number]['observation'][-1])
		#
		# 		force_obs = force_obs.reshape(len(self.buffer['obs']), -1)
		# 		force_obs_probability = 9 * (force_obs > 1).astype(np.float32) + 1
		#
		# 		force_obs_probability_episode = np.sum(force_obs_probability, axis=1)
		# 		force_obs_probability_episode = force_obs_probability_episode / np.sum(force_obs_probability_episode)
		# 		episode_idxs = np.random.choice(len(self.buffer['obs']), size=batch_size, p=force_obs_probability_episode)
		#
		# 		force_obs_probability_step = force_obs_probability[episode_idxs, :]
		# 		force_obs_probability_step = np.delete(force_obs_probability_step, -1, axis=1)
		# 		row_sums = np.sum(force_obs_probability_step, axis=1)
		# 		force_obs_probability_step = force_obs_probability_step / row_sums[:, np.newaxis]
		#
		# 		t_samples = np.array(
		# 			[np.random.choice(len(self.buffer['obs'][0])-1, size=1, p=force_obs_probability_step_row) for force_obs_probability_step_row
		# 			 in list(force_obs_probability_step)])
		# 		t_samples = np.array(t_samples).squeeze()
		#
		# 		batch['obs'].append(copy.deepcopy(obs))
		# 		batch['obs_next'].append(copy.deepcopy(obs_next))
		# 		batch['acts'].append(copy.deepcopy(act))
		# 		batch['rews'].append(copy.deepcopy([rew]))
		# 		batch['done'].append(copy.deepcopy(done))
		# 	else:
		# 		for key in ['obs', 'acts', 'rews', 'done']:
		# 			if key=='obs':
		# 				batch['obs'].append(copy.deepcopy(self.buffer[key][idx][step]))
		# 				batch['obs_next'].append(copy.deepcopy(self.buffer[key][idx][step+1]))
		# 			else:
		# 				batch[key].append(copy.deepcopy(self.buffer[key][idx][step]))
		#
		# return batch
